Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: B.A.T.M.A.N. advanced control and management tool
Upstream-Contact: Marek Lindner <mareklindner@neomailbox.ch>
Source: https://www.open-mesh.org/

Files: *
Copyright: 2007-2018, Andreas Langer <an.langer@gmx.de>
           2006-2018, Marek Lindner <mareklindner@neomailbox.ch>
           2006-2018, Simon Wunderlich <sw@simonwunderlich.de>
           2009-2018, Andrew Lunn <andrew@lunn.ch>
           2013-2018, Antonio Quartulli <a@unstable.cc>
License: GPL-2

Files: batman_adv.h
Copyright: 2016-2018, Matthias Schiffer <mschiffer@universe-factory.net>
License: MIT

Files: debugfs.*
Copyright: 2009, Clark Williams <williams@redhat.com>
	   2009, Xiao Guangrong <xiaoguangrong@cn.fujitsu.com>
License: GPL-2

Files: genl.*
Copyright: 2007-2008, Johannes Berg <Johannes Berg <johannes@sipsolutions.net>
	   2007, Andy Lutomirski <luto@kernel.org>
	   2007, Mike Kershaw
	   2008-2009, Luis R. Rodriguez <mcgrof@kernel.org>
License: ISC

Files: list.h
Copyright: 2012-2018, Sven Eckelmann <sven@narfation.org>
License: MIT

Files: debian/*
Copyright: 2008-2018, Simon Wunderlich <sw@simonwunderlich.de>
           2008-2018, Sven Eckelmann <sven@narfation.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: ISC
  Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
